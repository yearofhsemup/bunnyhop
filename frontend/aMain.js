// 
// 
// ****************** TODO *************************
// * [D]  Build a loading scene [as simple as possible]
// * [D]  Read Levedata from a datalayer stored in map
// * ...  [D] Create Datalayer in Tiled
// * ...  [D] How to read the data into phaser scene
// * ...  [D] Get first data for game setup
// * [D]  Level - set new time to ride
// * [1]  Commit the working progress
// * [D]  Restart after killbike if lifes>0
// * [D]  Show Level Flags response to value
// * [x]  show score count little longer
// * ...  [D] create some stages in count
// * ...  [x] fine tune the time
// * ...  [D] count bike bonus
// * ...  [D] display bike count
// * ...  [D] show new flag after levelup
// * [D]  Game Over
// * ...  [D] Game Over Text
// * ...  [D] Restart Option
// * [d]  Audio
// * ...  [x] Play SID music with plugin
// * ...  ... [x] Radio tune while driveing
// * ...  [D] Play Sound bytes
// * ...  ... [D] drive sound (faster and slower)
// * ...  ... [D] jump sound
// * ...  ... [D] landing sound
// * ...  ... [D] crashing sound
// * [x]  Sprites
// * ...  [D] Ballon with Chars
// * ...  ... [D] create anims
// * ...  ... [D] replace in order to write some
// * ...  ... [d] tune the anim readable
// 
let map;
let gameLogo;
let cursors;
let camera;
let debugGraphics;
let helpText;
let expSprite;
let player;
let sounds = {main:{},bounce:{},drive:{},fall:{},jump:{},coin:{},ballon:{},explode:{},gameover:{},badballon:{}};
const defaults = {bikeLeft:4}

let showDebug = false;

let dConf = {w:580,h:326,tileOff:{x:0,y:30}};
let goodGroup = [];
let badGroup = [];
let ballonGroup = [];
let cloudExpGroup = [];
let tweenz = [];
let level = [];

let pState = {
  gameOver:true,endGame:false,gamePaused:false,
  inAir:false,ramp:false,jump:70,catapult:130,
  drive:true,speed:52,maxSpeed:80,minSpeed:50,countScore:false,
  csState:"start",csTime:500,timeLeft:0
};

let hud = {
  lastUpdate:0,
  nextUpdate:500,
  scoreText: {},
  scoreValue: 0,
  highScoreText: {},
  highScoreValue: 1000,
  timeText: {},
  timeValue: 3,
  bonusText: {},
  bonusValue: 0,
  bikeLeftText: {},
  bikeLeftValue: defaults.bikeLeft,
  bikeLeftBonus: 0,
  bikeSprites: [],
  bonusMultiText: {},
  bonusMultiValue: 1,
  extraBikeText: {},
  gameOverText: {},
  levelImages: [],
  levelValue: 1,
  levelMaxValue: 18, // 18 max at 360 width; with offset 200
};

function preloadPreload() {
  let progress = this.add.graphics();
  this.load.on('progress', function (value) {
    progress.clear();
    progress.fillStyle(0xffffff, 1);
    progress.fillRect( (dConf.w/2) - ((dConf.w/2)*value), (dConf.h/5)*2, (dConf.w*value), (dConf.h/5));
  });
  this.load.on('complete', function () {
    progress.destroy();
    game.scene.start("mainMenu");
    // game.scene.start("mainGame");
    // game.scene.start("scoreHud");
  });

  // main game
  this.load.image('gameLogo', 'assets/images/GameLogo_001.png');
  this.load.tilemapTiledJSON('back', 'assets/maps/2021Set2022Map.json');
  this.load.image('Kickstart', 'assets/tiles/2021-testTiles--AB.png');
  this.load.image('Flag', 'assets/images/One_Flag.png');
  this.load.spritesheet('bike', 'assets/sprites/Biker_Anim.png',{ frameWidth: 18, frameHeight: 18 });
  this.load.spritesheet('explode', 'assets/sprites/ag4-explode.png',{ frameWidth: 32, frameHeight: 32 });
  this.load.spritesheet('cloudExp', 'assets/sprites/CloudExplode.png',{ frameWidth: 32, frameHeight: 32 });
  this.load.spritesheet('ballon','assets/sprites/BallonBlue--Slim.png',{ frameWidth: 24, frameHeight: 32 });
  this.load.spritesheet('revisionB','assets/sprites/BallChars.png',{ frameWidth: 18, frameHeight: 18 });
  this.load.spritesheet('skull','assets/sprites/Evil-b0T_ballon.png',{ frameWidth: 24, frameHeight: 32 });
  // audio
  this.load.audio('mainTheme', ['assets/audio/mainTitle.mp3','assets/audio/mainTitle.ogg']);
  this.load.audio('Bounceing', ['assets/audio/Bounceing.mp3','assets/audio/Bounceing.ogg']);
  this.load.audio('Driving', ['assets/audio/Driving.mp3','assets/audio/Driving.ogg']);
  this.load.audio('FallingIn', ['assets/audio/FallingIn.mp3','assets/audio/FallingIn.ogg']);
  this.load.audio('Jumping', ['assets/audio/Jumping.mp3','assets/audio/Jumping.ogg']);
  this.load.audio('Coining', ['assets/audio/Coining.mp3','assets/audio/Coining.ogg']);
  this.load.audio('Balloning', ['assets/audio/Balloning.mp3','assets/audio/Balloning.ogg']);
  this.load.audio('Exploding', ['assets/audio/Exploding.mp3','assets/audio/Exploding.ogg']);
  this.load.audio('gameOvering', ['assets/audio/gameOvering.mp3','assets/audio/gameOvering.ogg']);
  this.load.audio('badBalloning', ['assets/audio/badBalloning.mp3','assets/audio/badBalloning.ogg']);
  // hud assets
  this.load.bitmapFont('arcade', 'assets/fonts/bitmap/arcade.png', 'assets/fonts/bitmap/arcade.xml');
}

function mainGameCreate() {
  map = this.make.tilemap({ key: 'back', tileWidth: 8, tileHeight: 8});
  let tileset = map.addTilesetImage('2021Set','Kickstart',8, 8, 1, 2);
  let layer = map.createDynamicLayer(0, tileset, dConf.tileOff.x, dConf.tileOff.y);
  loadLevelDataFromMap();

  this.anims.create({
    key: 'bikeDrive',
    frames: this.anims.generateFrameNumbers('bike', {start:11, end:13}),
    frameRate: 3,
    repeat: -1
  });

  this.anims.create({
    key: 'bikeUp',
    frames: this.anims.generateFrameNumbers('bike', {start:0, end:5}),
    frameRate: 6,
    repeat: 0
  });

  this.anims.create({
    key: 'bikeDown',
    frames: this.anims.generateFrameNumbers('bike', {start:6, end:10}),
    frameRate: 3,
    repeat: 0
  });

  this.anims.create({
    key: 'explode',
    frames: this.anims.generateFrameNumbers('explode', {start:0, end:8}),
    frameRate: 16,
    hideOnComplete: true,
    repeat: 0
  });

  this.anims.create({
    key: 'cloudExp',
    frames: this.anims.generateFrameNumbers('cloudExp', {start:0, end:7}),
    frameRate: 16,
    hideOnComplete: true,
    repeat: 0
  });

  this.anims.create({
    key: 'ballon',
    frames: this.anims.generateFrameNumbers('ballon', {start:0, end: 6}),
    frameRate: 5,
    repeat: -1
  });

  this.anims.create({
    key: 'skull',
    frames: this.anims.generateFrameNumbers('skull', {start:0, end: 13}),
    frameRate: 5,
    repeat: -1
  });

  let charSpriteList = ['R','E','V','I','S','O','N','2','0'];
  let frameOffset = 0;

  for(let c=0,cl=charSpriteList.length; c < cl; c++) {
    this.anims.create({
      key: 'char_'+charSpriteList[c],
      frames: this.anims.generateFrameNumbers('revisionB', {start:frameOffset, end:frameOffset+13}),
      frameRate: 7,
      repeat: -1
    });
    frameOffset += 14;
  }

  let goodCount = 0; let badCount = 0;  let ballonCount = 0;
  let goodList = []; let badList = [];  let ballonList = [];

  layer.forEachTile(tile => {
    if(tile.index === 2) {
      ballonList.push({x:tile.x,y:tile.y});
      map.removeTileAt(tile.x, tile.y);
      ballonCount++;
    } else 
    if(tile.index === 3) {
      goodList.push({x:tile.x,y:tile.y});
      map.removeTileAt(tile.x, tile.y);
      goodCount++;
    } else if(tile.index === 4) {
      badList.push({x:tile.x,y:tile.y});
      map.removeTileAt(tile.x, tile.y);
      badCount++;
    }
  });
  console.dir({goodList,badList,ballonList});
  badGroup = this.physics.add.group({
    classType: enemiesPoolClass,
    maxSize: badCount,
  });
  goodGroup = this.physics.add.group({
    classType: charBallzPoolClass,
    maxSize: goodCount
  });
  ballonGroup = this.physics.add.group({
    classType: goodPoolClass,
    maxSize: ballonCount
  });

  cloudExpGroup = this.add.group({
    classType: cloudExpPoolClass,
    maxSize: goodCount+badCount+ballonGroup
  });

  let revision = "REVISION2022".split("");
  let revisionCount = 0;
  let sortGoodList = goodList.sort((a,b) => { return a.x - b.x });

  for(let g=0,gl=sortGoodList.length; g < gl; g++) {
    let repl = goodGroup.get();
    if(!repl) break;
    repl.anims.play('char_'+revision[revisionCount]);
    revisionCount++;
    if(revisionCount>=revision.length) revisionCount = 0;
    tweenz.push(repl);
    let neTile = repl.spawn(sortGoodList[g].x*8,sortGoodList[g].y*8);
  }

  for(let b=0,bl=badList.length; b < bl; b++) {
    let repl = badGroup.get();
    if(!repl) break;
    repl.anims.play('skull');
    tweenz.push(repl);
    let badGroupTile = repl.spawn(badList[b].x*8, badList[b].y*8);
  }

  for(let l=0,ll=ballonList.length; l < ll; l++) {
    let repl = ballonGroup.get();
    if(!repl) break;
    repl.anims.play('ballon');
    tweenz.push(repl);
    let ballonGroupTile = repl.spawn(ballonList[l].x*8, ballonList[l].y*8);
  }


  for(let t in tweenz) {
    let distM = Math.floor( (Math.random() * 5)  + 5);
    let holdM = Math.floor( (Math.random() * 10)  + 170);
    let duraM = Math.floor( (Math.random() * 100) + 2500);
    let upOdown = (Math.round(Math.random())===1)? "-=" : "+=";
    this.tweens.add({
      targets: tweenz[t],
      y: { value: upOdown+distM, duration: duraM,  hold: holdM },
      yoyo: true,
      repeat: -1,
      ease: 'Sine.easeInOut',
      paused: false
    });  
  }

  let staticMapLayer = map.convertLayerToStatic(layer);
  layer.setActive(false);
  staticMapLayer.setCollisionBetween(8, 175);
  // staticMapLayer.setCollisionByExclusion([0,1,2,3,4,5,6,7,16,17,18,19,20],false,hitTheGround);

  expSprite = this.add.sprite(-34,-34, 'explode', 1);
  expSprite.anims.load('explode');
  expSprite.anims.play('explode');

  player = this.physics.add.sprite(30,130, 'bike', 1);
  player.anims.load('bikeDown');
  player.anims.load('bikeUp');
  player.anims.load('bikeDrive');
  player.anims.play('bikeDrive');

  this.physics.add.collider(player, staticMapLayer, hitTheGround);
  this.physics.add.overlap(player, goodGroup, hitGoodGroup);
  this.physics.add.overlap(player, ballonGroup, hitGoodGroup);
  this.physics.add.overlap(player, badGroup, hitBadGroup);

  camera = this.cameras.main;
  camera.setBounds(0, 0, map.widthInPixels, map.heightInPixels);
  camera.startFollow(player);

  cursors = this.input.keyboard.createCursorKeys();
  debugGraphics = this.add.graphics();
  this.input.keyboard.on('keydown_C', function (event) {
    showDebug = !showDebug;
    drawDebug();
  });
}

function mainGameUpdate() {
  if(player.active && pState.drive) {
    if (cursors.left.isDown) {
      pState.speed--;
    } else if (cursors.right.isDown) {
      pState.speed++;
    }

    if (cursors.up.isDown) {
      if(!pState.inAir) {
        sounds.jump.play();
        player.body.setVelocityY(pState.jump*-1);
        pState.inAir=true;
        player.anims.play('bikeUp');
      }
    } else if (player.anims.currentAnim.key==='bikeUp' && player.body.velocity.y>=1) {
      player.anims.play('bikeDown');
    }

    if(pState.speed>pState.maxSpeed) {pState.speed=pState.maxSpeed;} else if(pState.speed<pState.minSpeed) {pState.speed=pState.minSpeed;}

    player.body.setVelocityX(pState.speed);
    sounds.drive.setRate( (pState.speed/30)+.2 );
  } else if(!pState.drive && !pState.gameOver) {
    if(pState.speed>=0) {
      player.body.setVelocityX(pState.speed--);
      if(pState.speed===1) {
        player.anims.stop(player.anims.getCurrentKey(),true,1);
        pState.countScore=true;
      }
    }
  }
}

function drawDebug () {
  debugGraphics.clear();
  if (showDebug) {
    // Pass in null for any of the style options to disable drawing that component
    map.renderDebug(debugGraphics, {
      tileColor: null, // Non-colliding tiles
      collidingTileColor: new Phaser.Display.Color(243, 134, 48, 200), // Colliding tiles
      faceColor: new Phaser.Display.Color(40, 39, 37, 255) // Colliding face edges
    });
  }
}

const hitTheGround = (bike, layer) => {
  if(!player.active) return;
  // if(pState.inAir) {
  //   pState.inAir=false;
  //   bike.anims.play('bikeDrive');
  // } else 
  if((layer.index===9 || layer.index===10) && pState.drive && !pState.ramp) {
    pState.drive = false;
  } else if( (layer.index>=49 && layer.index<=176) || (bike.body.blocked.right && layer.faceLeft) ) {
    killTheBike(bike);
  } else if(layer.index>=11 && layer.index<=16) {
    sounds.bounce.play();
    bike.body.setVelocityY(pState.catapult*-1);
    bike.anims.play('bikeUp');
    pState.inAir=true;
  } else if(layer.index>=26 && layer.index<=48) {
    pState.ramp = false;
    pState.inAir=false;
  } 
};

const reSpawnBike = (bike) => {
  if(hud.bikeLeftValue>=1 && hud.timeValue>0) {
    sounds.fall.play({delay:.3});
    hud.bikeLeftValue--;
    updateBikeDraw();
    bike.setActive(true);
    bike.setVisible(true);
    pState.drive=true;
    pState.inAir=true;
    bike.setPosition(bike.x,30);
  } else {
    playGameOver();
  }
};

const killTheBike = (bike) => {
  sounds.explode.play();
  bike.setVelocity(0);
  bike.setActive(false);
  bike.setVisible(false);
  expSprite.setVisible(true);
  expSprite.setPosition(bike.x,bike.y);
  expSprite.anims.play('explode');
  pState.drive=false;
  reSpawnBike(bike);
};

const playGameOver = () => {
  pState.gameOver=true;
  sounds.gameover.play({delay:.8,volume:11});
  sounds.badballon.play({delay:1.8});
};


const hitGoodGroup = (bike, ball) => {
  if(!bike.active||!ball.active) return;
  sounds.ballon.play();
  ball.body.setVelocity(0);
  let exp = cloudExpGroup.get();
  if(exp) {
    exp.spawn(ball.x,ball.y);
  }
  ball.destroy();
  hud.scoreValue += 50;
};

const hitBadGroup = (bike, skull) => {
  if(!bike.active||!skull.active) return;
  sounds.badballon.play({delay:.2});
  sounds.explode.play();
  skull.body.setVelocity(0);
  skull.destroy();
  killTheBike(bike);
};

const enemiesPoolClass = new Phaser.Class({
  Extends: Phaser.GameObjects.Sprite,
  initialize: function enemiesPoolClass(scene) {
    Phaser.GameObjects.Sprite.call(this, scene, -16, -16, "skull");
    this.anims.load("skull");
    this.anims.play("skull");
  },
  spawn: function(x,y) {
    this.setActive(true);
    this.setVisible(true);
    this.setPosition(x+8, y);
    this.body.moves = false;
    this.anims.play(this.anims.getCurrentKey(),true,1);
  }
});

const charBallzPoolClass = new Phaser.Class({
  Extends: Phaser.GameObjects.Sprite,
  initialize: function enemiesPoolClass(scene) {
    Phaser.GameObjects.Sprite.call(this, scene, -16, -16, "char_R");
    this.anims.load("char_0");
    this.anims.load("char_2");
    this.anims.load("char_N");
    this.anims.load("char_O");
    this.anims.load("char_S");
    this.anims.load("char_I");
    this.anims.load("char_V");
    this.anims.load("char_E");
    this.anims.load("char_R");
    this.anims.play("char_R");
  },
  spawn: function(x,y) {
    this.setActive(true);
    this.setVisible(true);
    this.setPosition(x+8, y);
    this.body.moves = false;
    this.anims.play(this.anims.getCurrentKey(),true,1);
  }
});

const goodPoolClass = new Phaser.Class({
  Extends: Phaser.GameObjects.Sprite,
  initialize: function goodPoolClass(scene) {
    Phaser.GameObjects.Sprite.call(this, scene, -16, -16, "ballon");
    this.anims.load("ballon");
    this.anims.play("ballon");
  },
  spawn: function(x,y) {
    this.setActive(true);
    this.setVisible(true);
    this.setPosition(x+8, y);
    this.body.moves = false;
    this.anims.play(this.anims.getCurrentKey(),true,1);
  }
});

const cloudExpPoolClass = new Phaser.Class({
  Extends: Phaser.GameObjects.Sprite,
  initialize: function cloudExpPoolClass(scene) {
    Phaser.GameObjects.Sprite.call(this, scene, -16, -16, "cloudExp");
    this.anims.load("cloudExp");
    this.anims.play("cloudExp");
  },
  spawn: function(x,y) {
    this.setActive(true);
    this.setVisible(true);
    this.setPosition(x, y);
    this.anims.play(this.anims.getCurrentKey(),true,1);
  }
});

const getNextLevel = () => {
  let nLevel = level.shift();
  if(nLevel===undefined) return;
  if(nLevel.Time===-1&&nLevel.Bonus===-1&&nLevel.Level===-1) {
    pState.endGame=true;
    pState.gameOver=true;
    return;
  }
  if(nLevel.Level===1) {
    hud.timeValue = nLevel.Time;
  } else {
    hud.timeValue = (pState.timeLeft + nLevel.Time);
  }
  hud.bonusMultiValue = nLevel.Bonus;
  hud.levelValue = nLevel.Level;
  updateLevelFlags();
};

const showScoreCounterText = (toShow) => {
  let aValue = (toShow)? 1 : 0;
  hud.bonusText.alpha = aValue;
  hud.bikeLeftText.alpha = aValue;
  hud.extraBikeText.alpha = aValue;
  hud.bonusMultiText.alpha = aValue;
};

const showGameOverText = (toShow) => {
  let sValue = (toShow)? 1 : 0;
  hud.gameOverText.alpha = sValue;
};

const updateBikeDraw = () => {
  let bikeCount = hud.bikeLeftValue;
  for(let b in hud.bikeSprites) {
    let nAl = (bikeCount>=1)? 1 : 0;
    hud.bikeSprites[b].alpha = nAl; 
    bikeCount--;
  }
};

const updateLevelFlags = () => {
  let flagCount = hud.levelValue;
  for(let f in hud.levelImages) {
    let nFl = (flagCount>=1)? 1 : 0;
    hud.levelImages[f].alpha = nFl;
    flagCount--;
  }
};

const loadLevelDataFromMap = () => {
  level = [];
  let levelProps = map.getObjectLayer("LevelProps").objects;
  for(let p in levelProps) {
    let tp = {};
    for(let pe in levelProps[p].properties) {
      tp[levelProps[p].properties[pe].name] = levelProps[p].properties[pe].value;
    }
    level.push(tp);
  }
  getNextLevel();
};

const updateTimeText = () => {
  let dTime = (" "+parseInt(hud.timeValue/60)).substr(-2) + ":" + ("0"+hud.timeValue%60).substr(-2);
  hud.timeText.setText(dTime);
};

const updateScoreText = () => {
  hud.scoreText.setText( "SCORE:"+("0000000"+hud.scoreValue).substr(-7) );
};

const updateHighScoreText = () => {
  hud.highScoreText.setText("HIGH:"+("0000000"+hud.highScoreValue).substr(-7));
};

const updateBonusCounts = (type) => {
  let text,val,target;
  if(type==='bikeLeftBonus') {
    text = "Bike left bonus ";
    val = hud.bikeLeftBonus;
    target = hud.bikeLeftText;
  } else if(type==='timeLeftBonus') {
    text = "BonusText Here  ";
    val = hud.timeValue;
    target = hud.bonusText;
  }
  let vStr = ('000'+val).substr(-3);
  target.setText(text+vStr);
};

function scoreHudCreate() {
  hud.timeText = this.add.bitmapText(28, 5, 'arcade', '0', 12).setTint(0xfffeb2);
  hud.scoreText = this.add.bitmapText(config.width-385, 5, 'arcade', '0', 12).setTint(0x238e13);
  hud.highScoreText = this.add.bitmapText(config.width-165, 5, 'arcade', '0', 12).setTint(0x5d006d);
  
  hud.bonusText = this.add.bitmapText(config.width/4.64, 70, 'arcade',    'BonusText Here  666', 16);
  hud.bikeLeftText = this.add.bitmapText(config.width/4.64, 96, 'arcade', 'Bike left bonus 000', 16);
  hud.extraBikeText = this.add.bitmapText(config.width/4.64, 122, 'arcade', 'Five bikes left still', 16);
  hud.bonusMultiText = this.add.bitmapText(480, 85, 'arcade', 'x1', 16);
  showScoreCounterText(false);

  hud.gameOverText = this.add.bitmapText(config.width/5, 64, 'arcade', '     Game Over\n\n\n s-start ! up-jump\n\nleft right to speed', 20).setTint(0xa24242);

  for(let l=0,ll=hud.bikeLeftValue; l < ll; l++) {
    let tSpr = this.add.image(20+(l*24),260, 'bike', 0);
    hud.bikeSprites.push(tSpr);
  }
  for(let m=0,ml=hud.levelMaxValue; m < ml; m++) {
    let tImg = this.add.image(200+(m*10),260, 'Flag', 0);
    hud.levelImages.push(tImg);
  }
  updateLevelFlags();

  this.input.keyboard.on('keydown', (eventNames, theEvent) => {
    // console.dir(eventNames,theEvent);
    if(pState.gameOver && eventNames.code==="KeyS") {
      sounds.main.stop('loop');
      sounds.fall.play();
      pState.gameOver=false;
      pState.endGame=false;
    } else if(eventNames.code==="KeyP") {
      if(this.scene.isActive("mainGame")) {
        this.scene.pause("mainGame");
        pState.gamePaused = true;
        sounds.drive.stop('loop');
        // this.scene.pause("scoreHud");
      } else {
        this.scene.resume("mainGame");
        sounds.drive.play('loop');
        pState.gamePaused = false;
        // this.scene.resume("scoreHud");
      }
    }
  });
}

function scoreHudUpdate(time,delta) {
  if( pState.gameOver ) {
    sounds.drive.stop('loop');
    
    if(hud.bonusText.alpha===1) {
      showScoreCounterText(false);
    }
    if(hud.gameOverText.alpha===0) {
      showGameOverText(true);
    }
    if(player.active) {
      player.setActive(false);
      player.setVisible(false);
    }
    if(!pState.drive) {
      pState.drive = true;
    }
    return;
  } else if(!player.active && !pState.gameOver) {
    loadLevelDataFromMap();
    hud.bikeLeftValue=defaults.bikeLeft;
    hud.scoreValue=0;
    player.setPosition(30,130);
    player.setActive(true);
    player.setVisible(true);
    showGameOverText(false);
    updateBikeDraw();
    updateLevelFlags();
    sounds.drive.play('loop');
  }

  if( !pState.gamePaused && (hud.lastUpdate + hud.nextUpdate <= time) ) { 
    updateScoreText();
    if(hud.scoreValue>hud.highScoreValue) hud.highScoreValue = hud.scoreValue;
    updateHighScoreText();
    if(pState.drive) {
      if(hud.timeValue>=0) {      
        updateTimeText();
        hud.timeValue-=1;
        hud.scoreValue++;
      } else {
        killTheBike(player);
      }      
    }
    hud.lastUpdate=time;
  }
  if( player.active && !pState.drive  && pState.countScore) {
    if(pState.csState==="start") { 
      sounds.drive.stop('loop');
      pState.csTime=time; 
      pState.csState="run";
      pState.timeLeft=hud.timeValue;
      if(hud.bikeLeftValue<=4) {
        hud.extraBikeText.setText("EXTRA BIKE WILL GIVEN ...");
        hud.bikeLeftValue+=1;
      } else {
        hud.extraBikeText.setText("FIVE BIKES LEFT STILL");
      }
      hud.bikeLeftBonus = (hud.bikeLeftValue * 25);
      updateBonusCounts('bikeLeftBonus');
      hud.bonusMultiText.setText(`${hud.bonusMultiValue}x`)
    }
    if(pState.csState==="run"  && (pState.csTime + 1000) < time) {
      if(hud.bonusText.alpha===0) {
        showScoreCounterText(true);
      }
      if(hud.timeValue>0) {
        hud.timeValue--;
        hud.scoreValue+= hud.bonusMultiValue;
        if(hud.timeValue%4===0) sounds.coin.play();
        updateScoreText();
        updateBonusCounts('timeLeftBonus');
      } else if(hud.timeValue===0 && hud.bikeLeftBonus>0) {
        hud.bikeLeftBonus--;
        hud.scoreValue+= hud.bonusMultiValue;
        if(hud.bikeLeftBonus%3===0) sounds.coin.play();
        updateScoreText();
        updateBonusCounts('bikeLeftBonus');
      } else if(hud.timeValue===0 && hud.bikeLeftBonus===0) {
        pState.csState="fade";
      }
    }
    if(pState.csState==="fade") { pState.csTime=time; pState.csState="end"; }
    if(pState.csState==="end" && (pState.csTime + 1500) < time) {
      showScoreCounterText(false);
      pState.drive = true;
      pState.speed = pState.minSpeed;
      pState.ramp = true;
      pState.csState = "start";
      updateBikeDraw();
      getNextLevel();
      sounds.drive.play('loop');      
    }
  }
  if(pState.endGame && player.x > (map.width*map.tileWidth)+100) {
    playGameOver();
  }
}

function menuCreate() {
  this.sound.audioPlayDelay = 0.1;
  this.sound.loopEndOffset = 0.05;
  this.sound.volume = .5;

  sounds.main = this.sound.add('mainTheme');
  sounds.bounce = this.sound.add('Bounceing');
  sounds.drive = this.sound.add('Driving');
  sounds.jump = this.sound.add('Jumping',{rate:1.2,volume:0.55});
  sounds.fall = this.sound.add('FallingIn');
  sounds.coin = this.sound.add('Coining');
  sounds.ballon = this.sound.add('Balloning');
  sounds.explode = this.sound.add('Exploding');
  sounds.gameover = this.sound.add('gameOvering');
  sounds.badballon = this.sound.add('badBalloning');
      
  let loopMarker = {name:'loop',start:0,duration:21.42,config:{ loop: true}};
  sounds.main.addMarker(loopMarker);
  sounds.drive.addMarker({name:'loop',config:{loop:true,volume:0.35}});

  gameLogo = this.add.image(config.width/2, config.height/2, 'gameLogo');
  gameLogo.setAlpha(0);
  this.input.on('pointerup', function (pointer) { stopStartScene(this, 'preGame', 'mainGame'); }, this);
  let spaceKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
  spaceKey.on('down', (key, event) => { stopStartScene(this, 'preGame', 'mainGame'); });
}

function menuUpdate() {
  let toGo = gameLogo.alpha;
  if(toGo>=1) {
    stopStartScene();
  } else {
    gameLogo.setAlpha(toGo+.002);
  }
}

const stopStartScene = () => {
  sounds.main.play('loop',{ delay: 0});
  game.scene.stop("mainMenu");
  game.scene.start("mainGame");
  game.scene.start("scoreHud");
};

const config = {
  type: Phaser.AUTO,
  width: dConf.w,
  height: dConf.h,
  pixelArt: true,
  backgroundColor: 0x000000,
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 100 },
    }
  },
  scale: {
    mode: 3,//Phaser.Scale.RESIZE,
    autoCenter: Phaser.Scale.CENTER_BOTH,
    width: dConf.w,
    height: dConf.h
  },
  scene: [
    { key: 'preload',  active: true,  preload: preloadPreload },
    { key: 'mainMenu', active: false, create: menuCreate,     update: menuUpdate },
    { key: 'mainGame', active: false, create: mainGameCreate, update: mainGameUpdate },
    { key: 'scoreHud', active: false, create: scoreHudCreate, update: scoreHudUpdate },
    ],
};

var game = new Phaser.Game(config);